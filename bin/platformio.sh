#!/bin/bash

volume_name=platformio-config
image_name=platformio

env_file=
if [ -f $PWD/.env ];  then env_file=$PWD/.env; fi

set_volume_config=
is_volume=$(podman volume ls | grep -c $volume_name)
if [ $is_volume -eq 1 ]; then
  set_volume_config="-v $volume_name:/root/.platformio"
fi

device=
if [ -e /dev/ttyUSB0 ]; then
    device="/dev/ttyUSB0"
fi
if [ "$PIO_UPLOAD_PORT" ]; then
    device=$PIO_UPLOAD_PORT
fi
if [ "$device" ]; then
    echo "Using upload port $device"
fi

podman run \
    --rm \
    --env-file=$env_file \
    --device=$device \
    --security-opt label=disable \
    -v $PWD:/workspace \
    $set_volume_config \
    -w /workspace \
    -it \
    $image_name \
    pio $@
