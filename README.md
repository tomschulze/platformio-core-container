# PlatformIO Core container environment
An OCI container environment for [PlatformIO](http://platformio.org/) Core adapted from https://github.com/sglahn/docker-platformio-core.

## Build
Run `buildah bud -t platformio` inside this project folder.

## Use
Add `bin/platformio.sh` to your `$PATH` and use it as a wrapper/alias for the `pio` command provided by the PlatformIO CLI. Or use the podman commands directly as shown below.

Further optional steps:
* Add .env file with variables to be exported to the container (see below)
* Add persistent config-volume (see below)

### Environment variables

In order to take environment variables into the platformio container, add a `.env` file to the project root and append `--env_file=$PWD/.env` to the `podman run` command. When using the `bin/platformio.sh` this step is already taken care of and not necessary.

```bash
#project_dir/.env
MY_VAR=test1234
MY_VAR2=http://bla.bla
```

## Keep Configuration
If you want to keep the downloaded packages, etc. you can save the PlatformIO configuration outside of the container. This is required if you want to use e.g., `pio device monitor`. You can do this by using the following commands:

```bash
# create the volume
podman volume create platformio-config

# bind the volume in the podman run commands (if not using the platformio.sh)
podman run --rm -v platformio-config:/root/.platformio ...
```

## Starting the container manually

### When not needing a connected device
```bash
podman run \
    --rm \
    -v $PWD:/workspace:z \
    -w /workspace
    platformio \
    pio run
```

### When flashing and monitoring serial output of a device
```bash
podman run --rm \
    --device=/dev/ttyUSB0 \
    --security-opt label=disable \
    -v $PWD:/workspace \
    -w /workspace
    platformio \
    pio device monitor
```
